/**
 * Blocks Drag and Drop 
 */
Drupal.behaviors.userDashboard = function (){

  //Draggable blocks
  $(".draggable").draggable({
    cursor: 'move',
      helper: 'clone',
    start : function (event,ui){
    }      
  });							

//set opacity
   $('.draggable').draggable('option', 'opacity', 0.5);

//setter
// $('.draggable').draggable('option', 'connectToSortable', '.droppable');



//Droppable areas
  $(".droppable").droppable({
    accept:'.draggable',
    drop: function (event, ui){
      dropped_block = ui.draggable;
	//console.log("dragagble "+ui.draggable+" this "+this.innerHTML);

      ui.draggable.appendTo($(this));
      block_id = ui.draggable.attr('id');

      $("#edit-region-"+block_id).val($(this).attr('id'));

      //Get all elements of the region
      blocks_list = $(this).get(0).childNodes;            

      //In a loop assign for every block it's weight -position in the loop-
      for(i=1; i< blocks_list.length; i++) {
        block_id = $(blocks_list[i]).attr("id");
       $('#edit-weight-'+block_id).val(i);  
      }
	//$(this).sortable("refresh");
        //return false; 			

    } 
  });



  //Blocks' close link
  $(".close").click(function(){
      block_id = $(this).attr('name');
      $("#"+block_id).appendTo($(".unset_blocks")).removeClass('dropped'); 
      $('#edit-weight-'+block_id).val(0);  
      $("#edit-region-"+block_id).val(0);
  });												

  //sorting the blocks

    $('.droppable').Sortable(
	{
	    accept : 'draggable',
	    helperclass : 'sorthelper',
	    activeclass : 'sortableactive',
	    hoverclass : 'sortablehover',
	    opacity: 0.8,
	    fx:200,
	    revert:true
	}
    );


};

 
