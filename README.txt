
-- SUMMARY --

This module allows the users to manage display of blocks in a dashboard.

-- REQUIREMENTS --

jquery_ui & jquery_update development


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* Enable the module in administer >> Modules.


-- CONFIGURATION --

* Customize the settings in administer >> Site configuration >> Userdashboard and
  enable all blocks that you want users to choose from.


-- USAGE --

You can use this module to display your blocks in different regions as you liked
To manage dashboard page use the link dashboard/%uid/manage
To view user dashboard page use the link dashboard/%uid
