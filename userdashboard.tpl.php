<?php

/**
 * @file userdashboard.tpl.php
 * Default theme implementation to wrap aggregator content.
 *
 * Available variables:
 * - $dashboard_region: contains all blocks region ids  
 * - $dashboard_blocks_subject:  contains blocks subject
 * - $dashboard_blocks_content: contains blocks content
  */


$output="<table class='maintable' width='100%'><tr>";
foreach($dashboard_region as $region_id=>$regions){
	switch($regions){
	case 'l11':
		$region1_content .="<h2>".$dashboard_blocks_subject[$region_id]."</h2>";
		$region1_content .=$dashboard_blocks_content[$region_id]."<br>";
		break;
	case 'l12':
		$region2_content .="<h2>".$dashboard_blocks_subject[$region_id]."</h2>";
		$region2_content .=$dashboard_blocks_content[$region_id]."<br>";
		break;
	case 'l13':
		$region3_content .="<h2>".$dashboard_blocks_subject[$region_id]."</h2>";
		$region3_content .=$dashboard_blocks_content[$region_id]."<br>";
		break;
	case 'l21':
		$region4_content .="<h2>".$dashboard_blocks_subject[$region_id]."</h2>";
		$region4_content .=$dashboard_blocks_content[$region_id];
		break;
	case 'l22':
		$region5_content .="<h2>".$dashboard_blocks_subject[$region_id]."</h2>";
		$region5_content .=$dashboard_blocks_content[$region_id];
		break;
	case 'l23':
		$region6_content .="<h2>".$dashboard_blocks_subject[$region_id]."</h2>";
		$region6_content .=$dashboard_blocks_content[$region_id];
		break;
	case 'l31':
		$region7_content .="<h2>".$dashboard_blocks_subject[$region_id]."</h2>";
		$region7_content .=$dashboard_blocks_content[$region_id];
		break;
	case 'l32':
		$region8_content .="<h2>".$dashboard_blocks_subject[$region_id]."</h2>";
		$region8_content .=$dashboard_blocks_content[$region_id];
		break;
	case 'l33':
		$region9_content .="<h2>".$dashboard_blocks_subject[$region_id]."</h2>";
		$region9_content .=$dashboard_blocks_content[$region_id];
		break;
 	} 
}

$output .="<td valign='top' width='33%'>$region1_content</td>";
$output .="<td valign='top' width='33%'>$region2_content</td>";
$output .="<td valign='top' width='33%'>$region3_content</td>";
$output .='</tr><tr>';

$output .="<td valign='top' width='33%'>$region4_content</td>";
$output .="<td valign='top' width='33%'>$region5_content</td>";
$output .="<td valign='top' width='33%'>$region6_content</td>";
$output .='</tr><tr>';

$output .="<td valign='top' width='33%'>$region7_content</td>";
$output .="<td valign='top' width='33%'>$region8_content</td>";
$output .="<td valign='top' width='33%'>$region9_content</td>";
$output .='</tr></table>';

print $output;